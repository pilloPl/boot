package io.pillopl.springboot.boot;

import io.pillopl.springboot.boot.Borrower;
import io.pillopl.springboot.boot.BorrowerRepository;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.junit4.SpringRunner;

@SpringBootTest
public class Zadanie4_BorrowerRepoTest {

	/**
	 * TASK
	 * Spraw by testy przeszly.
	 * Podpowiedz: z jaka baza próbuje się łączyć test? sprawdz pom.xml
	 */

	@Autowired
	BorrowerRepository borrowerRepository;


	@Test
	public void shouldSaveNewBorrowerAnna() {
		//given
		Borrower anna = new Borrower("anna");
		borrowerRepository.save(anna);

		//expect
		Assertions.assertThat(borrowerRepository.count()).isEqualTo(1);
		Assertions.assertThat(borrowerRepository.findAll().iterator().next().name).isEqualTo("anna");

	}

	@Test
	public void shouldSaveNewBorrowerCris() {
		//given
		Borrower kris = new Borrower("cris");
		long count = borrowerRepository.count();
		borrowerRepository.save(kris);

		//expect
		Assertions.assertThat(borrowerRepository.count()).isEqualTo(count +1);
		Assertions.assertThat(borrowerRepository.findByName("cris").iterator().next().name).isEqualTo("cris");
	}

	@Test
	public void shouldSaveNewBorrowerJohn() {
		//given
		Borrower john = new Borrower("john");
		long count = borrowerRepository.count();
		borrowerRepository.save(john);

		//expect
		Assertions.assertThat(borrowerRepository.count()).isEqualTo(count +1);
		Assertions.assertThat(borrowerRepository.findByName("john").iterator().next().name).isEqualTo("john");
	}


}
