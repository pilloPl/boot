package io.pillopl.springboot.boot;

import io.pillopl.springboot.boot.Borrower;
import io.pillopl.springboot.boot.BorrowerRegistrationService;
import io.pillopl.springboot.boot.BorrowerRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
public class Zadanie10_ChangeNameTest {

	/**
	 * TASK
	 * Ma byc zielono - trzeba cos dopisac w BorrowerRegistrationService, tak zeby odkomentowany kod zadzialal.
	 * Mozna dodac tez wiecej kodu :)
	 *
	 */
	@Autowired
	BorrowerRegistrationService borrowerRegistrationService;

	@Autowired
	BorrowerRepository borrowerRepository;


	@Test
	public void can_change_name() {
		//given
		Borrower anna = new Borrower("anna");
		borrowerRepository.save(anna);

		//when
		borrowerRegistrationService.changeRegistrationName(anna.id, "john");

		//then
		assertThat(borrowerRepository.findByName("anna").size()).isEqualTo(0);
		assertThat(borrowerRepository.findByName("john").size()).isEqualTo(1);


	}

}
