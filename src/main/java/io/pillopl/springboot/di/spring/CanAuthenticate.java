package io.pillopl.springboot.di.spring;

public interface CanAuthenticate {

    String authenticate(String user);
}
