package io.pillopl.springboot.di.spring;

import java.util.concurrent.TimeUnit;

public class PaymentTitleProvider {

    private Logger logger = new Logger();

    String loadTitle(String userName) {
        //kosztowne wołanie innego serwisu (np. wolnej bazy danych)
        try {
            TimeUnit.SECONDS.sleep(5);
        } catch (InterruptedException e) {
            logger.log(e.getMessage());
            return "error";
        }
        return "payment title";
    }
}
