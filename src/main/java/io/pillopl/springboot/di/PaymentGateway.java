package io.pillopl.springboot.di;



public class PaymentGateway {

    private final AuthenticationManager authenticationManager;
    private final BlueCashService blueCashService;
    private final PaymentDetailsProvider paymentDetailsProvider;

    public PaymentGateway(AuthenticationManager authenticationManager, BlueCashService blueCashService, PaymentDetailsProvider paymentDetailsProvider) {
        this.authenticationManager = authenticationManager;
        this.blueCashService = blueCashService;
        this.paymentDetailsProvider = paymentDetailsProvider;
    }

    String pay(String userName)  {
        String authentication = authenticationManager.authenticate(userName);
        if (authentication.equals("OK")) {
            String paymentData = paymentDetailsProvider.getDetailsFor(userName);
            blueCashService.call("http://blue.pay", paymentData);
            return "OK";
        } else {
            return "FALSE";
        }

    }
}
