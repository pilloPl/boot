package io.pillopl.springboot.boot.card;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
class WithdrawalComment {

    public WithdrawalComment() {

    }

    public WithdrawalComment(String comment) {

        this.comment = comment;
    }

    @Id
    @GeneratedValue
    private Long id;
    private String comment;

    String getComment() {
        return comment;
    }

}

