package io.pillopl.springboot.boot;


import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.UUID;

@Entity
class Borrower {

    Borrower() {
    }

    @Id
    UUID id = UUID.randomUUID();

    String name;

    Borrower(String name) {
        this.name = name;
    }

}
