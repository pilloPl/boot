package io.pillopl.springboot.boot.offer;

import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

@Repository
class OfferRepository {

    private final Map<Integer, Offer> offers = new HashMap<>();

    Collection<Offer> all() {
        return offers.values();
    }

    Offer byId(Integer id) {
        return offers.get(id);
    }

    void save(Offer o) {
        offers.put(o.getId(), o);
    }

}


