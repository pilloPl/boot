package io.pillopl.springboot.boundaries;

interface ClientAddressRemoteService {

    ClientAddress getByBorrowerId(BorrowerId borrowerId);
}
