package io.pillopl.springboot.boundaries;

import java.math.BigDecimal;
import java.util.List;

interface BorrowerWithdrawalsRemoteService {

    List<Withdrawal> getByBorrowerId(BorrowerId borrowerId);

    void informAboutNewPayment(BigDecimal amount);
}
